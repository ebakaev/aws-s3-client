package com.nexperia;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class S3Client {

    public static final Logger logger = LoggerFactory.getLogger(S3Client.class);

    String rootBucketName;
    String accessKeyValue;
    String secretKeyValue;
    Regions clientRegion = Regions.EU_WEST_1;

    public static final String PROP_DRAWING_S3_DESCRIPTIVE_TITLE = "description-title";
    public static final String PROP_DRAWING_S3_DOCUMENT_TYPE = "document-type";
    public static final String PROP_DRAWING_S3_PTW = "publish-to-web";
    public static final String PROP_DRAWING_S3_MAG_CODE = "mag-code";
    public static final String PROP_DRAWING_S3_CREATION_DATE = "creation-date";
    public static final String PROP_DRAWING_S3_PUBLICATION_ID = "publication-id";
    public static final String PROP_DRAWING_S3_SPECIFICATION_STATUS = "specification-status";

    public void releaseDocument(String documentName, InputStream content, String path, Map<String, String> properties) {
        if (checkIfDocumentCanBeSent(documentName)) {
            path = path.toLowerCase().replace(" ", "-");
            try {
                AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
                AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .withRegion(clientRegion)
                        .build();
                logger.debug("Result bucket: " + rootBucketName + path);
//            logger.debug("ak: " + accessKeyValue);
//            logger.debug("sk: " + secretKeyValue);
                ObjectMetadata objectMetadataToSave = new ObjectMetadata();
                properties.keySet().forEach(key -> {
                    objectMetadataToSave.addUserMetadata(key, properties.get(key));
                });
                s3Client.putObject(rootBucketName + path, documentName, content, objectMetadataToSave);
            } catch (AmazonServiceException e) {
                // The call was transmitted successfully, but Amazon S3 couldn't process
                // it, so it returned an error response.
                e.printStackTrace();
            } catch (SdkClientException e) {
                // Amazon S3 couldn't be contacted for a response, or the client
                // couldn't parse the response from Amazon S3.
                e.printStackTrace();
            }

            logger.debug("{} document has been sent to Amazon S3 '{}'", documentName, rootBucketName + path);
        }
    }

    public void makeDocumentObsolete(String documentName, String bucketName) {
        bucketName = bucketName.toLowerCase().replace(" ", "-");
        try {
            AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(clientRegion)
                    .build();

            s3Client.deleteObject(new DeleteObjectRequest(rootBucketName + bucketName, documentName));
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }

        logger.debug("{} rendition has been removed from Amazon S3", documentName);
    }

    public Map<String, Map<String, String>> findDocumentsInAWSByName(String bucketName, String path, String searchString) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(clientRegion)
                .build();
        ObjectListing objectListing = s3Client.listObjects(bucketName, path);
//        System.out.println(objectListing.getObjectSummaries().stream().peek(s3ObjectSummary -> System.out.println(s3ObjectSummary.getKey())));
        Map<String, Map<String, String>> result = new HashMap<>();
        objectListing.getObjectSummaries().forEach(s3ObjectSummary -> {
            String documentKey = s3ObjectSummary.getKey();
            if (documentKey.contains(path) && documentKey.contains(searchString)) {
                Map<String, String> s3Properties = getMetadata(bucketName, documentKey);
                Map<String, String> properties = new HashMap<>();
                properties.put(PROP_DRAWING_S3_DESCRIPTIVE_TITLE, s3Properties.getOrDefault(PROP_DRAWING_S3_DESCRIPTIVE_TITLE, "-"));
                properties.put(PROP_DRAWING_S3_DOCUMENT_TYPE, s3Properties.getOrDefault(PROP_DRAWING_S3_DOCUMENT_TYPE, "-"));
                properties.put(PROP_DRAWING_S3_PTW, s3Properties.getOrDefault(PROP_DRAWING_S3_PTW, "-"));
                properties.put(PROP_DRAWING_S3_MAG_CODE, s3Properties.getOrDefault(PROP_DRAWING_S3_MAG_CODE, "-"));
                properties.put(PROP_DRAWING_S3_CREATION_DATE, s3Properties.getOrDefault(PROP_DRAWING_S3_CREATION_DATE, "-"));
                properties.put(PROP_DRAWING_S3_PUBLICATION_ID, s3Properties.getOrDefault(PROP_DRAWING_S3_PUBLICATION_ID, "-"));
                properties.put(PROP_DRAWING_S3_SPECIFICATION_STATUS, s3Properties.getOrDefault(PROP_DRAWING_S3_SPECIFICATION_STATUS, "-"));
                result.put(s3ObjectSummary.getKey(), properties);
            }
        });

        return result;
    }

    public Map<String, String> getMetadata(String bucketName, String key) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(clientRegion)
                .build();
//        s3Client.getObjectMetadata("nexperia-pws-data-qa", "documents/eduard_lll/" + documentName);
        ObjectMetadata objectMetadata = s3Client.getObjectMetadata(bucketName, key);
        return objectMetadata.getUserMetadata();
    }

    public void downloadResource(String bucketName, String path, String documentName, File file) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(clientRegion)
                .build();
        GetObjectRequest request = new GetObjectRequest(bucketName, path + documentName);
        s3Client.getObject(request, file);
    }

    private boolean checkIfDocumentCanBeSent(String documentName) {
        List<String> restrictionParts = Arrays.asList("SOT","SOD","_S1","_S2","_S3","_S4","_S5","_S6","_S7","_S8","_S9");
        boolean canPass = true;
        if (documentName.endsWith(".zip")) {
            for (String restriction : restrictionParts) {
                if (documentName.contains(restriction)) {
                    canPass = false;
                    break;
                }
            }
        }

        if (canPass) {
            System.out.println("Document can be sent to AWS S3...");
        } else {
            System.out.println("Document can NOT be sent to AWS S3 according to name restrictions...");
        }
        return canPass;
    }

    public void setRootBucketName(String rootBucketName) {
        this.rootBucketName = rootBucketName;
    }

    public void setAccessKeyValue(String accessKeyValue) {
        this.accessKeyValue = accessKeyValue;
    }

    public void setSecretKeyValue(String secretKeyValue) {
        this.secretKeyValue = secretKeyValue;
    }

}

